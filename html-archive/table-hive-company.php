

<!DOCTYPE html>
<html>
<head>
    <title></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.27.8/js/jquery.tablesorter.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.27.8/js/jquery.tablesorter.widgets.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.27.8/js/widgets/widget-filter.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.27.8/js/jquery.tablesorter.combined.min.js"></script> -->
<script type="text/javascript" src="assets/sass/build/js/app-custom.js"></script>
<script type="text/javascript" src="assets/sass/build/js/app.js"></script>


<!-- <link rel="stylesheet" href="assets/css/themes/blue/style.css"> -->

<link rel="stylesheet" href="assets/sass/build/css/app.css">




 </head>
<body>



<!-- <div class="columnSelectorWrapper vertical">
    <input id="colSelect1" type="checkbox" class="hidden" />
    <label class="columnSelectorButton" for="colSelect1">Column Select</label>
    <div id="columnSelector" class="columnSelector">
        <label><input type="checkbox" id="filter-all" checked >All</label>
    <div>
        <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked>Company Info</label></h5>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Company</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Status</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>MRR</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Licence</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Employees</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Licence Variance</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Survey length</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Grace</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Questions in next survey</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
        </div>
        <div>
        <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked id='filter-current_survey'>Current Survey</label></h5>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Q</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of High Fives</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Suggestions</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Anonymous Message</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
        </div>
        <div>
        <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked>Previous Survey</label></h5>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Q</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of High Fives</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Suggestions</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Anonymous Message</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
        </div>
        <div>
        <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked >Past Surveys</label></h5>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Q</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of High Fives</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Suggestions</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Anonymous Message</label>
            <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
        </div>
    </div>
</div> -->

<table class="tablesorter custom-popup tablesorter-blue hasStickyHeaders">
    
    <thead>
        <tr class="top-headings" class="tableheading_level1 tablesorter-ignoreRow hasSpan">
          <th class="sorter-false columnSelector-disable">&nbsp;</th>
          <th class="sorter-false columnSelector-disable" id="companyInfo" colspan="13">Company Info</th>
<!--           <th class="sorter-false filter-current_survey" colspan="5">Current Survey</th>
          <th class="sorter-false" colspan="5">Previous Survey</th> -->
<!--           <th class="sorter-false" colspan="5">Past Surveys</th> -->
        </tr>
        <tr class="tableheading_level2 tablesorter-headerRow">
            <th data-priority="critical" class="companyInfo">Company</th>
            <th data-priority="critical" class="companyInfo" >Status</th>
            <th class="company companyInfo-child">RAG</th>
            <th data-priority="6" class="companyInfo-child">MRR</th>
            <th>Licence</th>
            <th>Employees</th>
            <th>Licence Variance</th>
            <th>Survey Length</th>
            <th>Grace</th>
            <th>SMS</th>
            <th>Suggestions</th>
            <th>High Fives</th>
            <th>Last Survey</th>
<!--             <th>Questions in Next Survey</th> -->
<!--             <th class="filter-current_survey">Response Rate</th> 
            <th class="filter-current_survey">Number of Q</th> 
            <th class="filter-current_survey">Number of High Fives</th> 
            <th class="filter-current_survey">Number of Suggestions</th> 
            <th class="filter-current_survey">Number of Anonymous Messages</th>   
            <th >Response Rate</th> 
            <th data-priority="3">Number of Q</th> 
            <th data-priority="3">Number of High Fives</th> 
            <th data-priority="3">Number of Suggestions</th> 
            <th data-priority="3">Number of Anonymous Messages</th>    -->
<!--             <th >Response Rate</th> 
            <th>Number of Q</th> 
            <th>Number of High Fives</th> 
            <th>Number of Suggestions</th> 
            <th >Number of Anonymous Messages</th>    -->        
        </tr>
    </thead>

    <tbody>

        <?php include 'local.php'; ?>

        <?php 

        $sql = "SELECT * FROM `bf_organization` WHERE 1";


        if(!$result = $db->query($sql)){

            die('There was an error running the query [' . $db->error . ']');

        } else {
            // var_dump($result->num_rows);
            while ($row = $result->fetch_array()) {

                echo "<tr>
                    <td class='company_name'>".($row['organization_name'])."</td>
                    <td class='status'>".($row['status'])."</td>
                    <td class='rag'></td>
                    <td class='mrr'></td>
                    <td class='licence'></td>
                    <td class='employees'>".($row['employee_count'])."</td>
                    <td class='licence_variance'></td>
                    <td class='survey_length'>".($row['frequency'])."</td>    
                    <td class='grace'>".($row['grace'])."</td>  
                    <td class='sms-enabled'>".($row['sms_enabled'])."</td>
                    <td class='suggestions'>".($row['suggestions'])."</td>
                    <td class='high_fives'>".($row['hive_fives'])."</td> 
                    <td class='last_survey_date'>".($row['survey_sent_at'])."</td>   
                </tr>";
            }

        }

        ?>


    </tbody>
</table>


<table class="tablesorter custom-popup tablesorter-blue hasStickyHeaders">
    
    <thead>
        <tr class="top-headings" class="tableheading_level1 tablesorter-ignoreRow hasSpan">
          <th class="sorter-false columnSelector-disable">&nbsp;</th>
          <th class="sorter-false columnSelector-disable" id="companyInfo" colspan="13">Company Info</th>
        </tr>
        <tr class="tableheading_level2 tablesorter-headerRow">
            <th data-priority="critical" class="companyInfo">Company</th>
            <th data-priority="critical" class="companyInfo" >Status</th>
            <th class="company companyInfo-child">RAG</th>
            <th data-priority="6" class="companyInfo-child">MRR</th>
            <th>Licence</th>
            <th>Employees</th>
            <th>Licence Variance</th>
            <th>Survey Length</th>
            <th>Grace</th>
            <th>SMS</th>
            <th>Suggestions</th>
            <th>High Fives</th>
            <th>Last Survey</th> 
        </tr>
    </thead>

    <tbody>

        <?php include 'local.php'; ?>

        <?php 

        $sql = "SELECT * FROM `hivt1_questionnaire_master` WHERE 1";


        if(!$result = $db->query($sql)){

            die('There was an error running the query [' . $db->error . ']');

        } else {
            // var_dump($result->num_rows);
            while ($row = $result->fetch_array()) {

                echo "<tr>
                    <td class='company_name'>".($row['questionnaire_name'])."</td>
                </tr>";
            }

        }

        ?>


    </tbody>
</table>

</body>
</html>

