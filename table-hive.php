<!DOCTYPE html>

    <head>
        <title>Mustard Dashboard</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
        <link rel="stylesheet" href="assets/css/app.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.27.8/js/jquery.tablesorter.js"></script> 
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.27.8/js/jquery.tablesorter.widgets.js"></script>
        <script type="text/javascript" src="assets/js/tablesorter-app.js"></script>
        <script type="text/javascript" src="assets/js/app.js"></script>

    </head>

    <body>

        <div class="columnSelectorWrapper vertical">

            <input id="colSelect1" type="checkbox" class="hidden" />
            
            <label class="columnSelectorButton" for="colSelect1">Column Select</label>
        
            <div id="columnSelector" class="columnSelector">
                
                <label><input type="checkbox" id="filter-all" checked >All</label>

                <div>
                    <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked id="filter-company_info">Company Info</label></h5>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Company</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Status</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>MRR</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Licence</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Employees</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Licence Variance</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Survey length</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Grace</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Questions in next survey</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
                </div>

                <div>
                    <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked id="filter-next_survey" >Next Survey</label></h5>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Q</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Start Date</label>
                </div>   

                <div>
                    <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked id='filter-current_survey'>Current Survey</label></h5>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Q</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of High Fives</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Suggestions</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Anonymous Message</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
                </div>
            
<!--                 <div>
                    <h5><label class="disabled"><input type="checkbox" class="filter-topLevel" disabled checked>Previous Survey</label></h5>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Q</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of High Fives</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Suggestions</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Number of Anonymous Message</label>
                    <label class="disabled"><input type="checkbox" class="filter-children" disabled checked>Response Rate</label>
                </div> -->

            </div>
        </div><!-- End of columnSelectorWrapper -->

        <div class="wrapper">

            <table class="tablesorter custom-popup tablesorter-blue hasStickyHeaders focus-highlight">
                
                <thead>
                    <tr class="top-headings" class="tableheading_level1 tablesorter-ignoreRow hasSpan">
                        <th class="sorter-false columnSelector-disable">&nbsp;</th>
                        <th class="sorter-false columnSelector-disable" id="company_info" colspan="12">Company Info</th>
                        <th class="sorter-false columnSelector-disable" id="next_survey" colspan="2">Next Survey</th>
                        <th class="sorter-false columnSelector-disable" id="current_survey" colspan="5">Current Survey</th>
                   <!-- <th class="sorter-false" colspan="5">Previous Survey</th> -
                        <th class="sorter-false" colspan="5">Past Surveys</th> -->
                    </tr>
                    <tr class="tableheading_level2 tablesorter-headerRow">
                        <th>Company</th>
                        <th class="filter-company_info">Status</th>
                        <th class="filter-company_info">RAG</th>
                        <th class="filter-company_info">MRR</th>
                        <th class="filter-company_info">Licence</th>
                        <th class="filter-company_info">Employees</th>
                        <th class="filter-company_info">Licence Variance</th>
                        <th class="filter-company_info">Survey Length</th>
                        <th class="filter-company_info">Grace</th>
                        <th class="filter-company_info">SMS</th>
                        <th class="filter-company_info">Suggestions</th>
                        <th class="filter-company_info">High Fives</th>
                        <th class="filter-company_info">Last Survey</th>
                        <th class="filter-next_survey">Questions in Survey</th>
                        <th class="filter-next_survey">Start Date</th>
                        <th class="filter-current_survey">Response Rate</th> 
                        <th class="filter-current_survey">Number of Q</th> 
                        <th class="filter-current_survey">Number of High Fives</th> 
                        <th class="filter-current_survey">Number of Suggestions</th> 
                        <th class="filter-current_survey">Number of Anonymous Messages</th>   
                   <!-- <th >Response Rate</th> 
                        <th data-priority="3">Number of Q</th> 
                        <th data-priority="3">Number of High Fives</th> 
                        <th data-priority="3">Number of Suggestions</th> 
                        <th data-priority="3">Number of Anonymous Messages</th>
                        <th >Response Rate</th> 
                        <th>Number of Q</th> 
                        <th>Number of High Fives</th> 
                        <th>Number of Suggestions</th> 
                        <th >Number of Anonymous Messages</th> -->        
                    </tr>
                </thead>

                <tbody>

                    <?php include 'local.php'; ?>

                    <?php 

                    // Select all fields from bf_organiaztion
                    $sql_bf_organization_all = "SELECT * FROM `bf_organization` WHERE 1";

                    if(!$valid_sql_bf_organization_all = $db->query($sql_bf_organization_all)){

                        die('There was an error running the query [' . $db->error . ']');

                    } else {

                        while ($row_bf_organization = $valid_sql_bf_organization_all->fetch_array()) {

                            // VARIABLES - TABLE ORGANISATION SLUGS
                            // ************************************
                            $organization_slug = $row_bf_organization['organization_slug'];
                            $company_employees_table = $organization_slug.'_employee';
                            $questions_master_table = $organization_slug.'_questions_master';
                            $questionnaire_master_table = $organization_slug.'_questionnaire_master';
                            $hive_five_table = $organization_slug.'_hive_five_referral';
                            $suggestions_table = $organization_slug.'_questionnaire_suggestions';
                            // ********************************************************************************************
                            // ********************************************************************************************


                            // 1. QUERY - NUMBER OF EMPLOYEES
                            // *******************************
                            $valid_company_employees_table = $db->query("SELECT * FROM $company_employees_table WHERE 1");
                            // Variable storing number of employees
                            $company_employees_table_row_count = $valid_company_employees_table->num_rows;
                            // ********************************************************************************************
                            // ********************************************************************************************


                            // ****************
                            // 2. NEXT SURVEY
                            // ****************

                            // 2.1 GET HIGHEST QUESTIONNAIRE NUMBER - slug_questions_master
                            // *************************************************************
                            $valid_sql_highest_questionnaire_id = $db->query("SELECT MAX(questionnaire_id) AS highestQuestionnaireId FROM $questions_master_table");

                                // Only add to table if this company has value
                            if($valid_sql_highest_questionnaire_id->num_rows) {
                               $valid_sql_highest_questionnaire_id = $valid_sql_highest_questionnaire_id->fetch_array();
                               // Variable storing highest questionnaire number
                               $valid_sql_highest_questionnaire_id_value = $valid_sql_highest_questionnaire_id['highestQuestionnaireId'];
                            }

                            // 2.2 COUNT NUMBER OF QUESTIONS IN HIGHEST QUESTIONNAIRE NUMBER - slug_questions_master
                            // **************************************************************************************
                            $valid_sql_highest_questionnaire_id_count = $db->query("SELECT * FROM $questions_master_table WHERE questionnaire_id=$valid_sql_highest_questionnaire_id_value");
                                // Variable storing number of questions in latest questionnaire
                            $valid_sql_highest_questionnaire_id_count_numrows = $valid_sql_highest_questionnaire_id_count->num_rows;

                            // 2.3. GET NEXT SURVEY DATE FOR HIGHEST QUESTIONNAIRE NUMBER - slug_questionnaire_master
                            // **************************************************************************************
                            $valid_sql_next_questionnaire_date = $db->query("SELECT * FROM $questionnaire_master_table WHERE questionnaire_id=$valid_sql_highest_questionnaire_id_value");

                                // Only add to table if this company has value else add null
                            if($valid_sql_next_questionnaire_date->num_rows) {
                                $valid_sql_next_questionnaire_date_row = $valid_sql_next_questionnaire_date->fetch_array();
                                // Variable storing start date of next questionnaire
                                $valid_sql_next_questionnaire_date_row_start_date = $valid_sql_next_questionnaire_date_row['questionnire_start_date'];
                            } else {
                                $valid_sql_next_questionnaire_date_row_start_date = null;
                            }
                            // ********************************************************************************************
                            // ********************************************************************************************


                            // ****************
                            // 3. CURRENT SURVEY
                            // ****************

                            // 3.1 GET QUESTIONNAIRE BEFORE HIGHEST QUESTIONNAIRE NUMBER  - slug_questions_master
                             // *********************************************************************************
                            $valid_sql_highest_questionnaire_id_minus1 = $db->query("SELECT MAX(questionnaire_id) AS secondHighestQuestionnaireId FROM $questions_master_table WHERE questionnaire_id < $valid_sql_highest_questionnaire_id_value");
                         
                            if($valid_sql_highest_questionnaire_id_minus1->num_rows) {
                               $valid_sql_highest_questionnaire_id_minus1 = $valid_sql_highest_questionnaire_id_minus1->fetch_array();
                               // Variable storing number of questionnaire before highest questionnaire number 
                               $valid_sql_highest_questionnaire_id_minus1_value = $valid_sql_highest_questionnaire_id_minus1['secondHighestQuestionnaireId'];
                            } else {
                                $valid_sql_highest_questionnaire_id_minus1_value = null;
                            }

                            // 3.2 COUNT NUMBER OF QUESTIONS IN QUESTIONNAIRE BEFORE HIGHEST QUESTIONNAIRE NUMBER- slug_questions_master
                            // **********************************************************************************************************
                            $valid_sql_highest_questionnaire_id_minus1_count = $db->query("SELECT * FROM $questions_master_table WHERE questionnaire_id=$valid_sql_highest_questionnaire_id_minus1_value");
                                // Variable storing number of questions in questionnaire before highest questionnaire number
                            $valid_sql_highest_questionnaire_id_minus1_count_numrows = $valid_sql_highest_questionnaire_id_minus1_count->num_rows;

                            // 3.3 COUNT NUMBER OF HIVE FIVES IN QUESTIONNAIRE BEFORE HIGHEST QUESTIONNAIRE NUMBER- slug_hive_five_referral
                            // ************************************************************************************************************
                            $valid_sql_current_hive_five_total_count = $db->query("SELECT * FROM $hive_five_table WHERE questionnaire_id=$valid_sql_highest_questionnaire_id_minus1_value");
                                // Variable storing numnber of hive fives in questionnaire before highest questionnaire number
                            $valid_sql_current_hive_five_total_count_numrows = $valid_sql_current_hive_five_total_count->num_rows;
                            // ********************************************************************************************
                            // ********************************************************************************************

                            // 3.4 COUNT NUMBER OF SUGGESTIONS IN QUESTIONNAIRE BEFORE HIGHEST QUESTIONNAIRE NUMBER- slug__questionnaire_suggestions
                            // ************************************************************************************************************
                            $valid_sql_current_suggestions_total_count = $db->query("SELECT * FROM $suggestions_table WHERE questionnaire_id=$valid_sql_highest_questionnaire_id_minus1_value");
                                // Variable storing number of suggestions in questionnaire before highest questionnaire number
                            $valid_sql_current_suggestions_total_count_numrows = $valid_sql_current_suggestions_total_count->num_rows;
                            // ********************************************************************************************
                            // ********************************************************************************************

                            // ******************
                            // OUTPUT TABLE ROWS
                            // ******************
                            echo "<tr>
                                    <td class='company_name'>".($row_bf_organization['organization_name'])."</td>
                                    <td class='status'>".($row_bf_organization['status'])."</td>
                                    <td class='rag'></td>
                                    <td class='mrr'></td>
                                    <td class='licence'></td>
                                    <td class='employees'>$company_employees_table_row_count</td>
                                    <td class='licence_variance'></td>
                                    <td class='survey_length'>".($row_bf_organization['frequency'])."</td>    
                                    <td class='grace'>".($row_bf_organization['grace'])."</td>  
                                    <td class='sms-enabled'>".($row_bf_organization['sms_enabled'])."</td>
                                    <td class='suggestions'>".($row_bf_organization['suggestions'])."</td>
                                    <td class='high_fives'>".($row_bf_organization['hive_fives'])."</td> 
                                    <td class='last_survey_date'>".($row_bf_organization['survey_sent_at'])."</td>   
                                    <td class='next_questions_in_survey'>$valid_sql_highest_questionnaire_id_count_numrows</td>   
                                    <td class='next_questionnaire_start_date'>$valid_sql_next_questionnaire_date_row_start_date</td>   
                                    <td class='current_response_rate'>Response rate</td>  
                                    <td class='current_questions_in_survey'>$valid_sql_highest_questionnaire_id_minus1_count_numrows</td>
                                    <td class='current_number_of_hive_fives'>$valid_sql_current_hive_five_total_count_numrows</td>  
                                    <td class='current_number_of_suggestions'>$valid_sql_current_suggestions_total_count_numrows</td>  
                                    <td class='current_anonymous_messages'>Anonymous Messages</td>  
                                </tr>";    
                            
                        } // close whiile loop for bf_organization fetch array

                    } // Close else bracket

                ?>

                </tbody>

            </table>

        </div><!-- End wrapper div -->

    </body>

</html>

